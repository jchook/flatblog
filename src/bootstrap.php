<?php 

include __DIR__ . '/../vendor/autoload.php';
include __DIR__ . '/functions.php';

/**
 * Catch-all exception handler. Assuming this catches everything now that
 * PHP 7.1 has "Throwable" errors. Has ultra-simplified code that doesn't
 * use any functions.php functions to avoid potential recursive fail.
 */
set_exception_handler(function($e){
	print error($e);
});
