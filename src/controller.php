<?php

// Get the request information
$uri = $_SERVER['REQUEST_URI'] ?? '/';

// Home
if (($uri === '/') || ($uri === '/index.html')) {
	return print layout('meta', [
		'content' => article('info/splash'),
	]);
}

// Articles
if ($p = route('/articles/{slug}.html')($uri)) {
	return print layout('meta', [
		'content' => article('articles/' . $p['slug']),
	]);
}

// Pages
if ($p = route('/info/{slug}.html')($uri)) {
	return print layout('meta', [
		'content' => article('info/' . $p['slug']),
	]);
}

// SCSS
if ($p = route('/assets/styles/{name}.css')($uri)) {
	$path = __DIR__ . '/assets/styles/' . $p['name'] . '.scss';
	if (file_exists($path)) {
		header('Content-Type: text/css');
		return print shell_exec('sassc ' . escapeshellarg($path));
	}
}

// Assets
if (substr($uri, 0, 8) === '/assets/') {
	if (file_exists(__DIR__ . $uri)) {
		throw new \Flatblog\AssetFound;
	}
}

// 404
throw new \Flatblog\NotFound;
