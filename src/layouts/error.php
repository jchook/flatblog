<?php

// If a status is provided, use it
if (isset($status)):
	http_response_code($status);
endif;

?>
<div id="error">
	<h1><?= $title ?? 'Unknown Error' ?></h1>
	<p><?= $message ?? 'Ah! We\'re so embarrassed!' ?></p>
	<p>
		Return to the <a href="/">homepage</a>?
	</p>
</div>
<pre>
	<?php print_r($error) ?>
</pre>
