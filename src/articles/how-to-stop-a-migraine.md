---
title: How to Stop a Migraine Before It Happens
author: Wesley Roberts
sources:
    - url: http://www.pbs.org/wgbh/nova/nature/dogs-sense-of-smell.html
      title: Dogs' Dazzling Sense of Smell
      date: 2012-10-04
      author: Peter Tyson
...

***P*erfect 10.** A kind of glowing shine radiates from Molly's stoic and practiced final stance on the gymnast's mat. "We're not supposed to smile," she had explained, "We stay completely focused, entranced. Coach says to become pure concentration, and let nirvana be the gymnast."

Until last year, Molly, now 19, was unable to pursue her love for gymnastics competitively. Brutal and debilitating migraine attacks, some lasting two or three days, would interrupt her practice and concentration, every single week.

"I remember my first migraine," Molly recounted for me. "At first you think you are going blind or something." She was only eight years-old when she had her first trip to the ER due to a severe migraine headache and temporary loss of vision.

"The thing I hate is that... there's nothing you can do. It's like a slow train," she says. "Once you know it's coming, it's too late."

Molly is one of the **billion** sufferers of migraine worldwide who feel that they are doomed to be ambushed by this unbearable pain for their entire lives. She, like other migraine sufferers spends 70% more than the average person for her health care, and with fleeting success at best.

Even after trying over 16 different medications and treatment options ranging from ß-blockers to chiropractic care, her weekly migraine attacks raged on.


## Meet Bo

Even through these bleak times, fate had it's way. Molly grew a love for volunteering at the local animal shelter, where she encountered Bo.

Bo is one of the most beautiful creatures you will ever see. He has a certainty in his step that only Hercules could carry, and rich golden locks that are truly something from a fairy tale. As a Golden Retriever, his calm, loyal temperament seems natural and instinctive. He is gifted, and he changes Molly's life forever.

<img src="/assets/images/bo-golden-retriever.jpg" />

Everyone knew about Molly's struggle with migraine before long, even Bo. "They really start to dominate your life," she added. Others at the shelter even accused her of feigning headaches to leave early instead of cleaning kennels. "Cleaning kennels is actually one of my favorite parts of the job," Molly told me, "You get the best one-on-one time with the animals and they love you for it."

Over the months, Molly and Bo learned each other's habits. For example, as she latched his kennel shut, sometimes Bo would land a forceful paw right on the rim of his water bowl, flipping it and spilling the water all over his bedding. He would of course get the extra bit of attention he sought, and a fresh bowl, but Molly soon noticed something odd about this behavior.

Bo only did this on days when Molly later got a headache. "He was trying to warn me", Molly told me. "He could tell it was coming."

---

# The Nose Knows

***H*umans have something** to the tune of 6 million scent receptors in our nose. It sounds like a lot, but dogs have **300 million**. That's 50x more sensory data being fed to the brain for processing. And, you guessed it, the olfactory bulb of a dog brain is proportionally 50x larger than that of a human.

"Dogs can smell better than we can see," explains Dr. William Harris of the Pennsylvania State College School of Biology. "Our eyes only give us a snapshot of what's around us. A dog's nose gives him a full story about his surroundings."

The architecture of a dog's nose is very different from ours. A small percent of the air that a dog breathes is captured in their olfactory cavity. The particles build-up inside this area, giving dogs the ability to detect odors at the parts-per-trillion scale, and over time.

To put the scale into perspective, Dr. Harris says it's like being able to smell a teaspoon of sugar dissolved in an Olympic sized swimming pool. "The dog nose is one of nature's most baffling technologies," he adds.

Was Bo able to smell the migraine attacks coming?


## Service Dogs

I took my studies outside the classroom and met with Sydnee Baker, an accomplished dog trainer in Seattle.

Sydnee has trained over 50 service animals, 19 of which were medical alert dogs. After completing her two year program, what the dogs learned was miraculous. They could smell their masters' medical condition, and provide an early warning.

For migraine, seizures, diabetes, and bipolar disorder, Sydnee uses saliva samples to train dogs to *smell* the episodes coming, sometimes days before they occur. "The scent is highly individualized," says Sydnee in the yard outside her training facility, "We have to collect fresh samples for every new client."

The fact of the matter, she tells me, is that most dogs already detect the condition naturally. Many of them also show changes in behavior. "Our business is really just teaching the dogs to communicate what they already know," she says with a glint of awe.


---

Molly is now known world-wide as one of the greatest in the sport and will compete for her first Olympic medal next summer.
