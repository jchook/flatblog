<?php

/**
 * Use this to compile the site down to HTML
 * for an extremely fast flat file blog
 */

include __DIR__ . '/bootstrap.php';

// Build homepage
build('/index.html');

// Build articles and pages
$ext = '.md';
$start = strlen(__DIR__);
$end = -strlen($ext);
$paths = array_merge(
	glob(__DIR__ . '/articles/*' . $ext),
	glob(__DIR__ . '/info/*' . $ext)
);
foreach ($paths as $path) {
	$uri = substr($path, $start, $end) . '.html';
	build($uri);
}

// Build errors
build('/errors/404.html');

// Build styles
build('/assets/styles/reset.css');
build('/assets/styles/main.css');

// Build Assets (copy for now, but eventually optimize them)
exec('cp -r ' . escapeshellarg(__DIR__ . '/assets') . ' ' . escapeshellarg(dirname(__DIR__) . '/dist/'));
