---
title: 'Article Ideas'
...

***T*he art of writing** requires a certain amount of planning and a similar measure of play instinct. You have to dive deep into the mystery of your enjoyment and fears, far beyond the depth of words and understanding, then surface nearly out of breath with an exotic yet familiar vitriol, a fresh yet somehow "just right" perspective.

Below are some ideas&mdash; seeds to plant in the soil of your mind, to water and care for regardless of their fate, and to grow into articles.


## AI

- Generating music that sounds like original Bach pieces
- Generating art from other art
- Generating tweets from ArXiv posts, etc
- Detecting ahrythmia, cancer, migriane, etc with AI
- Image recognition used for communal living -- who didn't wash their dishes?
- Use RNN to automatically determine news site biases


## Art

- Art project ideas
    - Digital art with on a screen with additional lighting that is extremely bright for the reflection of sunlight, etc. Pitch to monitor creators. Probably a huge ass liability.
    - "Hesitation: Not even once" campaign posters etc
    - Beautiful dog statue that squirts brown water out of its butt every so often. Add other features as needed.
    -
- Art vs Science -- when they split and why they're not really so different
- Psyart


## Books

- Recommendations
- Summaries and reviews


## Dogs

- General dog training best practices
- Service dog requirements and laws
- Medical alert dog training, breeds, etc
- Dog noses and the science of their incredible gift
    - Artistic rendering of what a dog might smell


## Programming

- Sass pattern/framework/snippet that makes it easy to use musical ratios in your colors, sizes, etc.
- Email. Works like notifications where "subject read = email read" UNLESS you tap once to suggest it's important, or "double-tap" it to read. Maybe fix the ux issue there but.. you get the point. Add ML to learn what is important to you.


## Philosophy

-

## Science

- Experiments
- Water boiling speed experiment
- Github for science data/experiments
- Plant Consciousness

## Sound vs Color

- Visually uniform color pallets
- Musical harmony and 1:1 mapping to color schemes
- Individual note mappings
- Musing about a 2-way algorithm which takes visually pleasing and moving works of art and translates them into equally emotionally- and visually-appealing works of music.
