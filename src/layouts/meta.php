<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link href="https://fonts.googleapis.com/css?family=Comfortaa:700|Source+Serif+Pro|Roboto:700" rel="stylesheet">
		<link rel="stylesheet" href="/assets/styles/reset.css">
		<link rel="stylesheet" href="/assets/styles/main.css">
		<title>meter.ai</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	</head>
	<body>
		<header>
			<h1></h1>
			<nav>
				<a href="/">Home</a>
			</nav>
		</header>
		<?= $content ?>
	</body>
</html>
