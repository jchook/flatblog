<?php 

include __DIR__ . '/bootstrap.php';

try {
	include __DIR__ . '/controller.php';
} catch (\Flatblog\AssetFound $e) {
	if (php_sapi_name() == 'cli-server') {
		return false; // drop back to cli dev server for static assets
	}
	throw $e;
}
