<?php

namespace Flatblog\Renderers;

use League\CommonMark\Block\Element\AbstractBlock;
use League\CommonMark\Block\Element\ListBlock;
use League\CommonMark\Block\Renderer\ListBlockRenderer as LeagueListBlockRenderer;
use League\CommonMark\ElementRendererInterface;
use League\CommonMark\HtmlElement;

class ListBlockRenderer extends LeagueListBlockRenderer {
	public function render(AbstractBlock $block, ElementRendererInterface $htmlRenderer, $inTightList = false) {
		if (!($block instanceof ListBlock)) {
			throw new \InvalidArgumentException('Incompatible block type: ' . get_class($block));
		}
		return new HtmlElement(
			'div', ['class' => 'block'],
			parent::render($block, $htmlRenderer, $inTightList)
		);
	}
}
