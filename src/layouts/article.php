<article>
	<header>
		
		<!-- Title -->
		<?php if (isset($title)): ?>
			<h1><span><?= $title ?></span></h1>
		<?php endif ?>

		<!-- Date -->
		<?php if (isset($date)): ?>
			<time datetime="<?= $date ?>">
				<?= date('m d, y', strtotime($date)) ?>
			</time>
		<?php endif ?>
		
		<!-- Author -->
		<?php if (isset($author)): ?>
			<?= layout('author', compact('author')) ?>
		<?php endif ?>
	
	</header>
	
	<!-- Content -->
	<?= $content ?>

</article>
