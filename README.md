# Flatblog

An exercise in Occam's razor.


## Features

- Ultra-fast and low-resource
- Extremely simple content management
- Supports rich content with minimal abstraction


## How It Works

- Write articles in Markdown, LaTeX, etc
- All content is "flattened" to plain old HTML
- Content is cached on the server and on the browser


## Code Philosophy

- No classes, except Exceptions and integrations.
- No $yield() callback in layouts. Only static content.
- In general, [YAGNI](http://wiki.c2.com/?YouArentGonnaNeedIt).

If you have a need for dynamic functionality, use JavaScript. If you need a backend database, do it in a separate repository.

We have no framework. We don't want one. This will not become one. It's a one-off site built specifically for this one purpose. Don't abstract it for ease or promotion.


## Requirements

- Vagrant
- VirtualBox


## Try It Out

Start up the vagrant box and run the `dev` command.

```sh
vagrant up
vagrant ssh dev
```

This will start up the ultra simple PHP built-in webserver on port 8888. Once live, you can visit [http://flatblog.local:8888/](http://flatblog.local:8888/).


## Build for Production

Again, on the vagrant VM, simply run the `build` command.

```sh
build
```


## Deploy in Production

Simply point your web server to the `dist` folder.

This alone should be extremely fast and cheap for even 10k concurrent viewers.

However, if you need even more speed, you can enable caching on both the client and the server via HTTP headers and server configuration (respectively).
