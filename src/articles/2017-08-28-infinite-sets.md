---
title: "Infinite Sets"
author: Wesley Roberts
date: 2017-08-28
...

- Continuum hypothesis
  - There is no cardinality between Aleph-0 and Aleph-1
    - Why are they different?
      - Because diagonal argument

