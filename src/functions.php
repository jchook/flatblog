<?php

/**
 * Render an article. Reads YAML from the top of the document.
 */
function article($name, $context = [], $conf = []) {
	extract($conf + [
		'layout' => 'article',
		'format' => 'markdown',
		'ext' => '.md',
	]);
	if (preg_match('[^a-zA-Z0-9\-\_]', $name))
		throw new \Flatblog\NotFound;
	$meta = [];
	$doc = file_get_contents(__DIR__ . '/' . $name . $ext);
	if (!$doc) 
		throw new \Flatblog\NotFound;
	if ((substr($doc, 0, 4) === "---\n") || (substr($doc, 0, 5) === "%YAML")) {
		$end = strpos($doc, "\n...\n", 4);
		$meta = yaml_parse(substr($doc, 0, $end + 5));
		$doc = substr($doc, $end + 6);
	}
	return layout($layout, $meta + [
		'content' => $format($doc),
	]);
}

/**
 * Build an HTML file based on a request URI
 */
function build($uri) {
	$_SERVER['REQUEST_URI'] = $uri;
	ob_start();
	try {
		include __DIR__ . '/controller.php';
	} catch (Exception $e) {
		print error($e);
	}
	$content = ob_get_clean();
	$path = dirname(__DIR__) . '/dist/' . trim($uri, '/');
	$dir = dirname($path);
	if (!is_dir($dir)) {
		mkdir($dir, 0775, true);
	}
	file_put_contents($path, $content);
}

/**
 * Render an error
 */
function error(Throwable $error) {
	if ($error instanceof \Flatblog\NotFound) {
		$title = 'Not Found';
		$message = 'The requested resource was not found.';
		$status = 404;
	}
	ob_start();
	include __DIR__ . '/layouts/error.php';
	$content = ob_get_clean();
	ob_start();
	include __DIR__ . '/layouts/meta.php';
	return ob_get_clean();
}

/**
 * Render a layout
 */
function layout($name, $context = []) {
	extract($context, EXTR_REFS);
	ob_start();
	include __DIR__ . '/layouts/' . $name . '.php';
	return ob_get_clean();
}

/**
 * Convert markdown to HTML
 */
function markdown($markdown) {
	$environment = \League\CommonMark\Environment::createCommonMarkEnvironment();
	$environment->addBlockRenderer('League\CommonMark\Block\Element\Heading', new \Flatblog\Renderers\HeadingRenderer());
	$environment->addBlockRenderer('League\CommonMark\Block\Element\ListBlock', new \Flatblog\Renderers\ListBlockRenderer());
	$converter = new \League\CommonMark\CommonMarkConverter([], $environment);
	return $converter->convertToHtml($markdown);
}

/**
 * Handy function for matching URI routes
 */
function route($tpl) {
	$tpl = str_replace('*', '(.*?)', $tpl);
	$regExp =
  	'#^' .
      preg_replace_callback(
      	'#\{([a-zA-Z0-9_-]+)\}#',
      	function($matches) {
      		// http://www.regular-expressions.info/named.html
          return '(?P<' . $matches[1] . '>[^/\\\?\s\&\.]+)';
        },
        $tpl
      ) .
    '$#'
  ;
  return function($str) use ($regExp) {
    if (!is_string($str)) return false;
    $matches = array();
    $matched = preg_match($regExp, $str, $matches);
    return $matches ?: $matched;
  };
}
