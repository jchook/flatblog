<?php

namespace Flatblog\Renderers;

use League\CommonMark\Block\Element\AbstractBlock;
use League\CommonMark\Block\Element\Heading;
use League\CommonMark\Block\Renderer\HeadingRenderer as LeagueHeadingRenderer;
use League\CommonMark\ElementRendererInterface;
use League\CommonMark\HtmlElement;

class HeadingRenderer extends LeagueHeadingRenderer {
	public function render(AbstractBlock $block, ElementRendererInterface $htmlRenderer, $inTightList = false) {
		if (!($block instanceof Heading)) {
			throw new \InvalidArgumentException('Incompatible block type: ' . get_class($block));
		}
		$tag = 'h' . $block->getLevel();
    $attrs = [];
    foreach ($block->getData('attributes', []) as $key => $value) {
        $attrs[$key] = $htmlRenderer->escape($value, true);
    }
    return new HtmlElement($tag, $attrs, '<span>' . $htmlRenderer->renderInlines($block->children()) . '</span>');
	}
}
